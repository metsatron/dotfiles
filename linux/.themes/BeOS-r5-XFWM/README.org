* BeOS-r5 for XFWM
[[./screenshot.png]]
** About
Recreated using screenshots of BeOS-r5 and support assets from previous BeOS inspired themes.
 - By Metsatron

*** Credits:
  - BeOS Again v1 
  - by [[https://www.opendesktop.org/member/490062/][antice]]

  - Haiku-ish Classic (just a b5 mod)
  - by [[timrburnham@gmail.com][nonesuch]]
