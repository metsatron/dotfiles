'use babel';

import NvimModeView from '../lib/nvim-mode-view';

describe('NvimModeView', () => {
  it('has one valid test', () => {
    expect('life').toBe('easy');
  });
});
